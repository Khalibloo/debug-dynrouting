# README #

Web UI that will call the services of the expertus-backend


### How do I get set up? ###

To run the project, install the yarn package manager
Go to the expertus-ui folder
Type 'yarn' on the command line to download the dependencies
Type 'yarn start' to run the project

* Languages and Tools
1. Visual Studio Code IDE on Ubuntu 18.04
2. TypeScript
3. React 
4. Antd for UI Components

* Heroku Deployment 
To push the project to Heroku:


### Roadmap of UI ###
* 0.2: Add basic wireframe for simple page
* 0.4: Pull Data from Services
* 0.6: Add Basic User login/logout
* 0.8: Polish Visually
* 1.0: Add js charting tools, add mobile responsiveness

### Who do I talk to? ###

jp
eternalsoul@gmail.com