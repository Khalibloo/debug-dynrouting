import globalConfig from './config';

// Add config for development environment and another one for live deployment
// local uses config.local.ts
// live uses config.ts

const conf = () => {
  if (process.env.NODE_ENV === "development") {
    const localConfig = require("./config.local").default;
    return { ...globalConfig, ...localConfig };
  }
  return globalConfig;
}
//setup client config data
export const config = conf();
