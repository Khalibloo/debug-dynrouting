import * as React from 'react';
import Link from 'umi/link';
import { Layout, Row, Col } from 'antd';
import BasicLayout from '../layouts/BasicLayout';
import ProfileCard from '@/components/ProfileCard';
import FinanceCard from '@/components/FinanceCard';
import DrawerMenu from '@/components/DrawerMenu';
import { config } from '@/global';

interface Props {
  activeMenu: string;
  welcome: any;
  profile: any;
}

export default class ProfilePage extends React.Component<Props, {}>{
  render() {
    const props = this.props;
    const content = props.welcome.content;
    const profile = props.profile;
    let blah;

    if (props.activeMenu === "hello") {
      blah = (
        <Row>{props.welcome.content}</Row>
      );
    }
    else if (props.activeMenu === "profile") {
      blah = (
        <Row><ProfileCard profile={props.profile} /></Row>
      );
    }
    else if (props.activeMenu === "finance") {
      blah = (
        <Row><FinanceCard transactions={props.profile} /></Row>
      );
    }

    return (
      <BasicLayout title={"Profile"}>
        <Layout.Content style={{ marginTop: 64, textAlign: 'center' }}>
          <Row>
            {blah}
          </Row>
          <Row>data here</Row>
        </Layout.Content>
      </BasicLayout>
    );

  }
}
