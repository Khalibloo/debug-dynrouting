import * as React from 'react';
import { Drawer, Menu, Button, Row, Col, Icon } from 'antd';
import Link from 'umi/link';
import logo from '../assets/expertus.svg';
import burgerIcon from '../assets/hamburger.svg';

interface Props {
  activeMenu: '',
}

interface State {
  visible: boolean;
}

export default class DrawerMenu extends React.Component<Props,State> {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };



  render() {
    const props = this.props;
    const menu = (
      <Menu mode="inline" style={{ width: '100%', border: 'none', textTransform: 'uppercase' }}>
        <Menu.Item key="0">
          <Link to="#"><div onClick={props.activeMenu='hello'}>Hello</div></Link>
        </Menu.Item>
        <Menu.Item key="1">
          <Link to="#" ><div onClick={props.activeMenu='profile'}>Profile</div></Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="#" ><div onClick={props.activeMenu='finance'}>Finance</div></Link>
        </Menu.Item>
      </Menu>
    );
    return (
      <div>
        <Button onClick={this.showDrawer} style={{ height: 64, border: 'none', boxShadow: 'none' }}>
          <img src={burgerIcon} />
        </Button>
        <Drawer
          className="nav-drawer"
          title={
            <Row type="flex">
              <Col md={12}>
                <Button onClick={this.onClose} style={{ height: '100%', border: 'none' }}>
                  <Icon type="close" style={{ fontSize: '1.8em' }} />
                </Button>
              </Col>
              <Col md={12}>
                <Link to="/"><img src={logo} /></Link>
              </Col>
            </Row>
          }
          placement="left"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
        >
        {menu}
      </Drawer>
      </div>
    );
  }
}
