import * as React from 'react';
import { Layout, Row, Col} from 'antd';
import Link from 'umi/link';
import logo from '../assets/expertus.svg';
import customBg from '../assets/background.svg';
import DrawerMenu from '@/components/DrawerMenu';
import { url } from 'inspector';

interface Props {
  pathname: string,
  activeMenu: ''
}
interface State { }

export default class NavBar extends React.Component<Props, State>{
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <Layout.Header style={{ position: 'fixed', zIndex: 1, width: '100%', padding: 0, backgroundImage: customBg }}>
        <Row type="flex" justify="space-between" align="middle" style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Col md={1} style={{ height: '100%' }}>
            <DrawerMenu activeMenu={this.props.activeMenu}/>
          </Col>
          <Col md={23} style={{ paddingLeft: 10, paddingRight: 10 }}>
            <Link to="/">
              <img src={logo} /> Expertus
            </Link>
          </Col>
        </Row>
      </Layout.Header >
    );
  }
}
