import * as React from 'react';
import Link from 'umi/link';
import { Layout, Row, Col, Form, Button, Input } from 'antd';
import RootLayout from '../layouts/RootLayout';

import { config } from '@/global';
import ProfilePage from './ProfilePage';

interface Props {
  profile: any,
  email: string[]
}
interface State { }

export default class HomePage extends React.Component<Props, State>{

  render() {
      const props = this.props;
      return (
        <RootLayout title={"Home"}>
          <Layout.Content style={{ marginTop: 64, textAlign: 'center' }}>
            <Row>
              <Col md={8}>
                &nbsp;
              </Col>
              <Col md={8}>
                <Link to={`${props.profile}${props.email[0]}`} >Hinton Watts</Link>
              </Col>
              <Col md={8}>
                &nbsp;
              </Col>
            </Row>
            <Row>
              <Col md={8}>
                &nbsp;
              </Col>
              <Col md={8}>
                <Link to={`${props.profile}${props.email[1]}`}>Sherri Greer</Link>
              </Col>
              <Col md={8}>
                &nbsp;
              </Col>
            </Row>
          </Layout.Content>
        </RootLayout>
      );
  }
}
