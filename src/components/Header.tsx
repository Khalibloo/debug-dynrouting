import withRouter from 'umi/withRouter';
import NavBar from './NavBar';

interface Props{
  location?: {pathname: string},
  activeMenu: '',
}

const Header = (props: Props) => (
  <NavBar pathname={props.location.pathname} activeMenu={props.activeMenu} />
)

export default withRouter(Header)
