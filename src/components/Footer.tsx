import * as React from 'react';
import { Layout } from 'antd';

interface Props{}
export default class Footer extends React.Component<Props, {}>{
  render() {
    return (
      <Layout.Footer style={{ textAlign: 'center', padding: 0, marginTop: 30 }}>
        <div style={{backgroundColor: '#bbb',height: 50}}>
          Expertus Mock App ©2018 All Rights Reserved
        </div>
      </Layout.Footer>
    );
  }
}
