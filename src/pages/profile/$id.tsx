import * as React from 'react';
import ProfilePage from '@/components/ProfilePage';
import axios from 'axios';
import HomePageSkeleton from '@/components/HomePageSkeleton';

interface Props {
  match?: any,
  activeMenu: '',
}
interface State { }
export default class Home extends React.Component<Props, State>{
  state = {
    email: null,
    welcome: null,
    profile: null,
  }
  componentDidMount() {
    axios.get(`http://localhost:8080/hello`)
      .then(res => {
        const welcome = res.data;
        this.setState({ welcome });
      });
    axios.get(`http://localhost:8080/profile/${this.props.match.params.id}`)
      .then(res => {
        const profile = res.data;
        this.setState({ profile });
      });
  }
  render() {
    const props = this.props;
    if (this.state.welcome == null || this.state.profile == null)
    {
        return <HomePageSkeleton />
    }
    return <ProfilePage welcome={this.state.welcome} profile={this.state.profile} activeMenu={this.props.activeMenu} />
  }
}
