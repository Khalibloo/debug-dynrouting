//local config data for running the project in dev
export default {
    siteName: "Expertus UI",
    useMediaBaseUrl: false,
    mediaBaseUrl: "http://localhost:8000",
  }
